# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-01 00:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "translator-credits"
msgstr ""

#: data/app_id.appdata.xml data/app_id.desktop

#. Application name
msgid "Tracks"
msgstr ""

#. Application short description / summary
msgid "A simple audio player"
msgstr ""

#. Generic name (.desktop)
msgid "Audio player"
msgstr ""

#. Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
msgid "audio;music;player;stream;track;"
msgstr ""

msgid "New instance"
msgstr ""

#: data/app_id.gschema.xml

msgid "Use title bar"
msgstr ""

msgid "This will take effect on restart"
msgstr ""

msgid "Display covers in background"
msgstr ""

msgid "Fade background covers"
msgstr ""

msgid "Display the spectral playing indicator"
msgstr ""

msgid "Increases CPU usage"
msgstr ""

msgid "Play continuously"
msgstr ""

msgid "Remember playlist"
msgstr ""

msgid "Close the window"
msgstr ""

msgid "About application"
msgstr ""

msgid "Preferences"
msgstr ""

msgid "Properties"
msgstr ""

msgid "Keyboard Shortcuts"
msgstr ""

msgid "Fullscreen"
msgstr ""

msgid "New window"
msgstr ""

msgid "Add files to playlist"
msgstr ""

msgid "Load playlist…"
msgstr ""

msgid "Save playlist…"
msgstr ""

msgid "Search in the playlist"
msgstr ""

msgid "Show active track"
msgstr ""

msgid "Display controls"
msgstr ""

msgid "Toggle play/pause"
msgstr ""

msgid "Go backward 5 seconds"
msgstr ""

msgid "Go forward 15 seconds"
msgstr ""

#: src/js/application.js

msgid "Print version information and exit"
msgstr ""

msgid "Open new window"
msgstr ""

msgid "Open new instance"
msgstr ""

msgid "When adding a folder, browse it recursively to add files"
msgstr ""

#: src/js/dialogs.js

msgid "Select music files"
msgstr ""

msgid "Audio files"
msgstr ""

msgid "Video files"
msgstr ""

msgid "All files"
msgstr ""

#:-already in src/js/application.js
#:-msgid "When adding a folder, browse it recursively to add files"
#:-msgstr ""

msgid "Enter file or stream address"
msgstr ""

msgid "Address entry"
msgstr ""

msgid "Export the playlist"
msgstr ""

msgid "Playlists"
msgstr ""

msgid "Select the playlist"
msgstr ""

#. e.g. "5 tracks" or "30 seconds"
msgid "%d %s"
msgstr ""

msgid "track"
msgid_plural "tracks"
msgstr[0] ""
msgstr[1] ""

#. playlist length and playlist save date/time (e.g. "5 tracks, 10:36")
msgid "%s, %s"
msgstr ""

msgid "Delete the playlist"
msgstr ""

msgid "Load a playlist"
msgstr ""

msgid "Enter a name to save the playlist with"
msgstr ""

msgid "Save entry"
msgstr ""

msgid "Save the current playlist"
msgstr ""

#:-already in data/app_id.gschema.xml
#:-msgid "Preferences"
#:-msgstr ""

msgid "Display"
msgstr ""

msgid "Player"
msgstr ""

msgid "Covers"
msgstr ""

msgid "Cover"
msgstr ""

#:-already in data/app_id.gschema.xml
#:-msgid "Properties"
#:-msgstr ""

msgid "Informations"
msgstr ""

msgid "General"
msgstr ""

msgid "Audio"
msgstr ""

msgid "Description"
msgstr ""

msgid "Podcast"
msgstr ""

msgid "Comment"
msgstr ""

msgid "Lyrics"
msgstr ""

#. %s is a tag name (properties dialog)
msgid "%s:"
msgstr ""

msgid "Playlist"
msgstr ""

#: src/js/headerbar.js

#:-already in data/app_id.gschema.xml
#:-msgid "Toggle play/pause"
#:-msgstr ""

msgid "Position"
msgstr ""

msgid "Adjust track position"
msgstr ""

msgid "Mute"
msgstr ""

#. %d is the volume (e.g. "50 %")
msgid "%d %"
msgstr ""

#:-already in data/app_id.gschema.xml
#:-msgid "Properties"
#:-msgstr ""
#:-msgid "Load playlist…"
#:-msgstr ""
#:-msgid "Save playlist…"
#:-msgstr ""
#:-msgid "New window"
#:-msgstr ""
#:-msgid "Fullscreen"
#:-msgstr ""
#:-msgid "Preferences"
#:-msgstr ""
#:-msgid "Keyboard Shortcuts"
#:-msgstr ""

#. %s is the application name
msgid "About %s"
msgstr ""

msgid "Skip backward"
msgstr ""

msgid "Shuffle playlist"
msgstr ""

msgid "Skip forward"
msgstr ""

msgid "Sort playlist descending"
msgstr ""

msgid "Empty playlist"
msgstr ""

msgid "Sort playlist ascending"
msgstr ""

#:-already in data/app_id.gschema.xml
#:-msgid "Add files to playlist"
#:-msgstr ""

#: src/js/view.js

msgid "Title"
msgstr ""

msgid "Playing indicator"
msgstr ""

#. "artists / album"
msgid "%s / %s"
msgstr ""

msgid "Artists"
msgstr ""

msgid "Album"
msgstr ""

msgid "Duration"
msgstr ""

msgid "Unkown"
msgstr ""

#:-already in src/js/dialogs.js
#:-msgid "Playlist"
#:-msgstr ""

msgid "Remove track"
msgstr ""

msgid "Track properties"
msgstr ""

#: src/js/scanner.js

#:-already in src/js/dialog.js
#:-msgid "%d %s"
#:-msgstr ""

msgid "hour"
msgid_plural "hours"
msgstr[0] ""
msgstr[1] ""

msgid "minute"
msgid_plural "minutes"
msgstr[0] ""
msgstr[1] ""

msgid "second"
msgid_plural "seconds"
msgstr[0] ""
msgstr[1] ""

#. %s is the previous "%d %s" (e.g. "2 minutes 30 seconds")
msgid "%s %s"
msgstr ""

#. %s is the previous "%d %s" (e.g. "1 hour 2 minutes 30 seconds")
msgid "%s %s %s"
msgstr ""

msgid "Channels"
msgstr ""

#. e.g. "Surround 5.1"
msgid "%s %d.1"
msgstr ""

msgid "Surround"
msgstr ""

msgid "Stereo"
msgstr ""

msgid "Mono"
msgstr ""

msgid "Sample rate"
msgstr ""

msgid "%d Hz"
msgstr ""

msgid "%d kbps"
msgstr ""

msgid "Media extraction from the website failed.\n%s not found."
msgstr ""

#: src/js/window.js

#:-already in src/js/dialogs.js
#:-msgid "Cover"
#:-msgstr ""

msgid "Filter the playlist"
msgstr ""

msgid "Add dragged file to the playlist"
msgstr ""
