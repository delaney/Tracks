/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported ServerManager */

const { Gio, GLib } = imports.gi;
// Optional, not compatible with Gtk4.
const GnomeDesktop = pkg.checkSymbol('GnomeDesktop') ? imports.gi.GnomeDesktop : null;
const GioOverrides = imports.overrides.Gio;
const Signals = imports.signals;
const System = imports.system;

const OBJECT_PATH = '/org/mpris/MediaPlayer2';
const BASE_NAME = 'org.mpris.MediaPlayer2';
const BASE_IFACE = 'org.mpris.MediaPlayer2';
const PLAYER_IFACE = 'org.mpris.MediaPlayer2.Player';
const TRACKLIST_IFACE = 'org.mpris.MediaPlayer2.TrackList';
const PLAYLISTS_IFACE = 'org.mpris.MediaPlayer2.Playlists';

const TRACK_UPDATE_TIMEOUT = 100; // ms.
const TRACKLIST_MAX_LENGTH = 50;

// Manage thumbnails for Player and TrackList servers.
const thumbnailizer = {
    _covers: new WeakMap(),
    _queried: new WeakSet(),
    _uris: new WeakMap(),

    query: function(track) {
        if (this._queried.has(track))
            return;

        this._queried.add(track);

        let file = Gio.File.new_for_uri(track.uri);
        // Do not query info for 'https', ...
        if (!file.is_native())
            return;

        let fileInfo = file.query_info('thumbnail::path,thumbnail::is-valid', Gio.FileQueryInfoFlags.NONE, null);
        if (fileInfo.has_attribute('thumbnail::path') && fileInfo.get_attribute_boolean('thumbnail::is-valid')) {
            let thumbnailPath = fileInfo.get_attribute_as_string('thumbnail::path');

            // Flatpak issue workaround
            if (thumbnailPath.indexOf('/.var/app/') != -1) {
                let cacheDir = Gio.File.new_for_path(GLib.get_user_cache_dir());
                let thumbnailFile = Gio.File.new_for_path(thumbnailPath);
                let relativePath = cacheDir.get_relative_path(thumbnailFile);
                thumbnailPath = GLib.build_filenamev([GLib.get_home_dir(), '.cache', relativePath]);
            }

            this._uris.set(track, GLib.filename_to_uri(thumbnailPath, null));
            this.emit('track-thumbnailized', track);
        }
    },

    generate: function(track, cover) {
        if (this._uris.has(track) || !GnomeDesktop || this._covers.get(track) == cover)
            return;

        this._covers.set(track, cover);

        let file = Gio.File.new_for_uri(track.uri);
        // Do not query info for 'https', ...
        if (!file.is_native())
            return;

        let thumbnailPath;
        let attributes = 'standard::content-type,time::modified' + (!this._queried.has(track) ? ',thumbnail::path,thumbnail::is-valid' : '');
        let fileInfo = file.query_info(attributes, Gio.FileQueryInfoFlags.NONE, null);

        if (!this._queried.has(track)) {
            this._queried.add(track);
            if (fileInfo.has_attribute('thumbnail::path') && fileInfo.get_attribute_boolean('thumbnail::is-valid'))
                thumbnailPath = fileInfo.get_attribute_as_string('thumbnail::path');
        }

        if (!thumbnailPath) {
            this.thumbnailFactory = this.thumbnailFactory || GnomeDesktop.DesktopThumbnailFactory.new(GnomeDesktop.DesktopThumbnailSize.LARGE);
            let [uri, mimeType, mtime] = [track.uri, fileInfo.get_content_type(), fileInfo.get_modification_date_time() && fileInfo.get_modification_date_time().to_unix()];

            if (this.thumbnailFactory.can_thumbnail(uri, mimeType, mtime)) {
                let pixbuf = cover.getPixbuf(256);
                if (pixbuf) {
                    this.thumbnailFactory.save_thumbnail(pixbuf, uri, mtime);
                    thumbnailPath = this.thumbnailFactory.lookup(uri, mtime);
                }
            }
        }

        if (thumbnailPath) {
            this._uris.set(track, GLib.filename_to_uri(thumbnailPath, null));
            this.emit('track-thumbnailized', track);
        }
    },

    getUri: function(track) {
        return this._uris.get(track);
    }
};
Signals.addSignalMethods(thumbnailizer);

// To know which properties mpris clients get.
const originalHandlePropertyGet = GioOverrides._handlePropertyGet;
GioOverrides._handlePropertyGet = function(info, impl, propertyName) {
    // this is a Server instance.
    this.queriedPropertyNames?.add(propertyName);

    return originalHandlePropertyGet.call(this, info, impl, propertyName);
};

// Abstract class.
const Server = class {
    handlers = new Map();
    queriedPropertyNames = new Set();

    constructor(application, iface) {
        this.application = application;
        this.window = this.application.active_window;
        this.application.connect('notify::active-window', this._onActiveWindowChanged.bind(this));

        // Gio.DBusExportedObject: Gjs override.
        let [success_, contents] = Gio.File.new_for_uri(`resource://${application.resource_base_path}/dbus-interfaces/${iface}.xml`).load_contents(null);
        let ifaceXml = contents.toLiteral();
        this.implementation = Gio.DBusExportedObject.wrapJSObject(ifaceXml, this);
        this.interfaceInfo = this.implementation.get_info();
        this.exported = false;
    }

    export(connection, path) {
        this.implementation.export(connection, path);
    }

    unexport(connection) {
        this.implementation.unexport();
    }

    _changeWindow(window) {
        this.window = window;
        this.handlers.forEach(({ source, id }) => source.disconnect(id));
        this.handlers.clear();
        // Do not emit 'PropertyChanged' for properties that interests no client.
        this.queriedPropertyNames.forEach(this._emitPropertyChanged.bind(this));
    }

    _onActiveWindowChanged() {
        let activeWindow = this.application.active_window;

        if (activeWindow && activeWindow != this.window)
            if (activeWindow.get_mapped())
                this._changeWindow(activeWindow);
            else
                activeWindow.connect('map', this._changeWindow.bind(this));
    }

    _invalidateProperty(propertyName) {
        let variant = new GLib.Variant('(sa{sv}as)', [this.interfaceInfo.name, {}, [propertyName]]);
        Gio.DBus.session.emit_signal(null, OBJECT_PATH, 'org.freedesktop.DBus.Properties', 'PropertiesChanged', variant);
    }

    _getPropertyVariant(propertyName) {
        let propertyInfo = this.interfaceInfo.lookup_property(propertyName);
        let jsValue = this[propertyName];
        if (jsValue !== undefined && jsValue !== null)
            return new GLib.Variant(propertyInfo.signature, jsValue);
        else
            log(`${propertyName}: no value`);
        return null;
    }

    _emitPropertyChanged(propertyName) {
        let variant = this._getPropertyVariant(propertyName);
        if (variant)
            this.implementation.emit_property_changed(propertyName, variant);
    }

    _emitAllPropertyChanged() {
        this.implementation.get_info().properties.map(property => property.name)
                                                 .forEach(propertyName => this._emitPropertyChanged(propertyName));
    }

    _bindProperty(source, sourcePropertyName, interfacePropertyName, handlerName) {
        if (!this.handlers.has(handlerName))
            this.handlers.set(handlerName, {
                source,
                id: source.connect(`notify::${sourcePropertyName}`, this._emitPropertyChanged.bind(this, interfacePropertyName)),
            });
    }

    _bindSignal(source, sourceSignalName, callback, handlerName) {
        if (!this.handlers.has(handlerName))
            this.handlers.set(handlerName, {
                source,
                id: source.connect(sourceSignalName, callback),
            });
    }
};

const BaseServer = class extends Server {
    constructor(application) {
        super(application, BASE_IFACE);
    }

    Raise() {
        this.window.present();
    }

    Quit() {
        this.window.destroy();
    }

    get CanQuit() {
        return true;
    }

    get Fullscreen() {
        this._bindProperty(this.window, 'fullscreened', 'Fullscreen', 'Fullscreen0');
        return this.window.fullscreened;
    }

    set Fullscreen(fullscreen) {
        this.window.fullscreened = fullscreen;
    }

    get CanSetFullscreen() {
        return true;
    }

    get CanRaise() {
        return true;
    }

    get HasTrackList() {
        return true;
    }

    get Identity() {
        return pkg.shortName;
    }

    get DesktopEntry() {
        return pkg.name;
    }

    get SupportedUriSchemes() {
        return ['file', 'http', 'https', 'smb', 'dav', 'davs', 'trash'];
    }

    get SupportedMimeTypes() {
        return ['application/ogg', 'audio/x-vorbis+ogg', 'audio/x-flac', 'audio/mpeg'];
    }
};

// Abstract class with convenient methods for Player and TrackList servers.
const TrackServer = class extends Server {
    _trackIds = new WeakMap();

    get player() {
        return this.window.player;
    }

    get selection() {
        return this.window.selection;
    }

    _getTrackId(track) {
        if (!this._trackIds.has(track))
            this._trackIds.set(track, `${this.application.resource_base_path}/track${System.addressOf(track).slice(2)}`);

        return this._trackIds.get(track);
    }

    _getMetadata(track) {
        let metadata = {
            'mpris:trackid': GLib.Variant.new('o', this._getTrackId(track)),
            'mpris:length': GLib.Variant.new('x', track.duration),
            'xesam:url': GLib.Variant.new('s', track.uri),
            'xesam:title': GLib.Variant.new('s', track.title),
            'xesam:album': GLib.Variant.new('s', track.album),
            'xesam:artist': GLib.Variant.new('as', track.artist.split(';')),
            'xesam:albumArtist': GLib.Variant.new('as', track.albumArtist.split(';')),
            'xesam:trackNumber': GLib.Variant.new('i', track.trackNumber),
            'xesam:genre': GLib.Variant.new('as', track.genre.split(';'))
        };

        if (thumbnailizer.getUri(track))
            metadata['mpris:artUrl'] = GLib.Variant.new('s', thumbnailizer.getUri(track));
        else
            thumbnailizer.query(track);

        return metadata;
    }
};

const PlayerServer = class extends TrackServer {
    constructor(application) {
        super(application, PLAYER_IFACE);
    }

    Next() {
        this.selection.skipForward();
    }

    Previous() {
        this.selection.skipBackward();
    }

    Pause() {
        this.player.paused = true;
    }

    PlayPause() {
        this.player.paused = !this.player.paused;
    }

    Stop() {
        this.player.paused = true;
    }

    Play() {
        this.player.paused = false;
    }

    Seek(offset) {
        let newPosition = this.player.position + offset;
        if (newPosition <= this.player.duration)
            this.player.position = newPosition;
    }

    /*
     * MPRIS specifications:
     * “If this does not match the id of the currently-playing track, the call is ignored as "stale".”
     */
    SetPosition(trackId, position) {
        if (!this.selection.activeTrack || this._getTrackId(this.selection.activeTrack) != trackId)
            return;

        this.player.position = position;
    }

    OpenUri(uri) {
        this.window.scanner.scanFiles([Gio.File.new_for_uri(uri)], false);
    }

    get PlaybackStatus() {
        this._bindProperty(this.player, 'active', 'PlaybackStatus', 'PlaybackStatus0');
        this._bindProperty(this.player, 'paused', 'PlaybackStatus', 'PlaybackStatus1');
        return !this.player.active ?
            'Stopped' :
            this.player.paused ? 'Paused' : 'Playing';
    }

    // eslint-disable-next-line getter-return
    get LoopStatus() { // 'Playlist', 'Track' or 'None'
        // Not implemented;
    }

    set LoopStatus(loopStatus) {
        // Not implemented;
    }

    get Rate() {
        return 1.0;
    }

    set Rate(rate) {
        // Should not be used since MinimumRate and MaximumRate both return 1.0.
    }

    get Shuffle() {
        this._bindProperty(this.selection, 'shuffle', 'Shuffle', 'Shuffle0');
        return this.selection.shuffle;
    }

    set Shuffle(shuffle) {
        this.selection.shuffle = shuffle;
    }

    get Metadata() {
        this._bindProperty(this.selection, 'active-track', 'Metadata', 'Metadata0');
        this._bindSignal(this.player, 'notify::cover', player => {
            if (this.selection.activeTrack && this.player.cover)
                thumbnailizer.generate(this.selection.activeTrack, player.cover);
        }, 'Metadata1');
        this._bindSignal(thumbnailizer, 'track-thumbnailized', (thumbnailizer_, track) => {
            if (this.selection.activeTrack == track)
                GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
                    this._emitPropertyChanged('Metadata');
                    return GLib.SOURCE_REMOVE;
                });
        }, 'Metadata2');

        let track = this.selection.activeTrack;
        if (!track)
            return { 'mpris:trackid': GLib.Variant.new('o', '/org/mpris/MediaPlayer2/TrackList/NoTrack') };

        return this._getMetadata(track);
    }

    get Volume() {
        this._bindProperty(this.player, 'volume', 'Volume', 'Volume0');
        return this.player.volume;
    }

    set Volume(volume) {
        this.player.volume = volume;
    }

    /*
     * MPRIS specifications:
     * “The 'org.freedesktop.DBus.Properties.PropertiesChanged' signal is NOT emitted when this property changes.”
     */
    get Position() {
        // "Inconsistant" position changes are indicated with the 'Seeked' signal.
        this._bindSignal(this.player, 'seeked', () => {
            this.implementation.emit_signal('Seeked', GLib.Variant.new('(x)', [this.Position]));
        }, 'Position0');
        return this.player.position;
    }

    get MinimumRate() {
        return 1.0;
    }

    get MaximumRate() {
        return 1.0;
    }

    get CanGoNext() {
        this._bindProperty(this.selection, 'for-skippable', 'CanGoNext', 'CanGoNext0');
        return this.selection.forSkippable;
    }

    get CanGoPrevious() {
        this._bindProperty(this.selection, 'back-skippable', 'CanGoPrevious', 'CanGoPrevious0');
        return this.selection.backSkippable;
    }

    get CanPlay() {
        this._bindProperty(this.player, 'active', 'CanPlay', 'CanPlay0');
        return this.player.active;
    }

    get CanPause() {
        this._bindProperty(this.player, 'active', 'CanPause', 'CanPause0');
        return this.player.active;
    }

    get CanSeek() {
        return this.player.seekable;
    }

    /*
     * MPRIS specifications:
     * “The 'org.freedesktop.DBus.Properties.PropertiesChanged' signal is NOT emitted when this property changes.”
     */
    get CanControl() {
        return true;
    }
};

/*
 * MPRIS specifications:
 * “Provides access to a short list of tracks which were recently played or will be played shortly.
 * This is intended to provide context to the currently-playing track, rather than giving complete
 * access to the media player's playlist.”
 *
 * Implementation:
 * Expose a reduced (TRACKLIST_MAX_LENGTH as maximum length) list around the active track.
*/
const TrackListServer = class extends TrackServer {
    constructor(application) {
        super(application, TRACKLIST_IFACE);
    }

    _changeWindow(window) {
        if (this._tracks)
            this._updateTracks();
        super._changeWindow(window);
    }

    _getTrack(trackId) {
        // Bypass useless stuff.
        if (trackId == '/org/mpris/MediaPlayer2/TrackList/NoTrack')
            return null;

        for (let track of this._tracks)
            if (this._getTrackId(track) == trackId)
                return track;

        return null;
    }

    _onMetadataChanged(thumbnailizer_, track) {
        if (this._tracks.includes(track)) {
            let trackId = this._getTrackId(track);
            GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
                this.implementation.emit_signal('TrackMetadataChanged', GLib.Variant.new('(oa{sv})', [trackId, this._getMetadata(track)]));
                return GLib.SOURCE_REMOVE;
            });
        }
    }

    // updateTrackTimeout: suppose MPRIS client cannot handle concurrent tracklist updates.
    // e.g. "mprisindicatorbutton@JasonLG1979.github.io" GNOME Shell extension.
    _updateTracks() {
        if (this._updateTrackTimeout)
            GLib.source_remove(this._updateTrackTimeout);

        this._updateTrackTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT_IDLE, TRACK_UPDATE_TIMEOUT, () => {
            let oldTracks = this._tracks || [];
            let tracks = [...this.selection];
            let activeTrack = this.selection.activeTrack && tracks.includes(this.selection.activeTrack) ? this.selection.activeTrack : null;
            let activeIndex = activeTrack ? tracks.indexOf(activeTrack) : 0;
            let maxBegin = tracks.length - TRACKLIST_MAX_LENGTH;
            let begin = Math.max(Math.min(activeIndex - Math.floor(TRACKLIST_MAX_LENGTH / 2), maxBegin), 0);
            let end = begin + TRACKLIST_MAX_LENGTH;
            this._tracks = tracks.slice(begin, end);

            let removedTracks = oldTracks.filter(track => !this._tracks.includes(track));
            let addedTracks = this._tracks.filter(track => !oldTracks.includes(track));

            // 'TrackListReplaced': Suppose MPRIS client cannot handle concurrent 'TrackRemoved' or 'TrackAdded' signals.
            // e.g. "mprisindicatorbutton@JasonLG1979.github.io" GNOME Shell extension.
            if (removedTracks.length > 1 || addedTracks.length > 1) {
                let trackIds = this._tracks.map(track => this._getTrackId(track));
                let currentTrack = activeTrack ? this._getTrackId(activeTrack) : '/org/mpris/MediaPlayer2/TrackList/NoTrack';
                this.implementation.emit_signal('TrackListReplaced', GLib.Variant.new('(aoo)', [trackIds, currentTrack]));
                this._invalidateProperty('Tracks');
            } else if (removedTracks.length && addedTracks.length) {
                this.implementation.emit_signal('TrackRemoved', GLib.Variant.new('(o)', [this._getTrackId(removedTracks[0])]));
                GLib.timeout_add(GLib.PRIORITY_DEFAULT_IDLE, 10, () => {
                    let metadata = this._getMetadata(addedTracks[0]);
                    let previousTrack = this._tracks[this._tracks.indexOf(addedTracks[0]) - 1] || null;
                    let previousTrackId = previousTrack ? this._getTrackId(previousTrack) : '/org/mpris/MediaPlayer2/TrackList/NoTrack';
                    this.implementation.emit_signal('TrackAdded', GLib.Variant.new('(a{sv}o)', [metadata, previousTrackId]));
                    this._invalidateProperty('Tracks');
                    return GLib.SOURCE_REMOVE;
                });
            } else if (removedTracks.length) {
                this.implementation.emit_signal('TrackRemoved', GLib.Variant.new('(o)', [this._getTrackId(removedTracks[0])]));
                this._invalidateProperty('Tracks');
            } else if (addedTracks.length) {
                let metadata = this._getMetadata(addedTracks[0]);
                let previousTrack = this._tracks[this._tracks.indexOf(addedTracks[0]) - 1] || null;
                let previousTrackId = previousTrack ? this._getTrackId(previousTrack) : '/org/mpris/MediaPlayer2/TrackList/NoTrack';
                this.implementation.emit_signal('TrackAdded', GLib.Variant.new('(a{sv}o)', [metadata, previousTrackId]));
                this._invalidateProperty('Tracks');
            }

            this._updateTrackTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    GetTracksMetadata(trackIds) {
        this._bindSignal(thumbnailizer, 'track-thumbnailized', this._onMetadataChanged.bind(this), 'GetTracksMetadata0');

        return trackIds
            .map(trackId => this._getTrack(trackId))
            .filter(track => track != null)
            .map(track => this._getMetadata(track));
    }

    AddTrack(uri, afterTrackId, setAsCurrent) {
        let afterTrack = this._getTrack(afterTrackId);
        let index = afterTrack ? [...this.selection].indexOf(afterTrack) + 1 : 0;
        this.window.scanner.scanFiles([Gio.File.new_for_uri(uri)], false, index, setAsCurrent);
    }

    RemoveTrack(trackId) {
        let track = this._getTrack(trackId);
        if (track != null)
            this.selection.removeTrack(track);
    }

    GoTo(trackId) {
        let track = this._getTrack(trackId);
        if (track != null)
            this.selection.activeTrack = track;
    }

    /*
     * MPRIS specifications:
     * “When this property changes, the 'org.freedesktop.DBus.Properties.PropertiesChanged' signal is emitted,
     * but the new value is not sent.”
     */
    get Tracks() {
        this._bindSignal(this.selection, 'items-changed', this._updateTracks.bind(this), 'Tracks1');
        this._bindSignal(this.selection, 'notify::active-track', this._updateTracks.bind(this), 'Tracks2');

        if (!this._tracks)
            this._updateTracks();

        return (this._tracks || []).map(track => this._getTrackId(track));
    }

    get CanEditTracks() {
        return true;
    }
};

const PlaylistsServer = class extends TrackServer {
    constructor(application) {
        super(application, PLAYLISTS_IFACE);
        this.playlists = application.playlists;
    }

    _getPlaylistId(name) {
        return `${this.application.resource_base_path}/playlist${GLib.base64_encode(name)}`.replace(/=/gi, '_');
    }

    _getPlaylistName(playlistId) {
        let code = playlistId.slice(`${this.application.resource_base_path}/playlist`.length);
        return GLib.base64_decode(code.replace(/_/gi, '=')).toLiteral();
    }

    ActivatePlaylist(playlistId) {
        this.selection.playlist = this.playlists.getPlaylist(this._getPlaylistName(playlistId));
        this.selection.activatePlaylist(true);
    }

    GetPlaylists(index, maxCount, order, reverseOrder) {
        return [...this.playlists].sort((playlist1, playlist2) => {
            let compare = order == 'Alphabetical' ?
                playlist1.displayName > playlist2.displayName :
                playlist1.modificationDateTime.to_unix() > playlist2.modificationDateTime.to_unix();
            return reverseOrder ? -compare : compare;
        }).slice(index, maxCount).map(playlist => [this._getPlaylistId(playlist.name), playlist.displayName, '']);
    }

    get PlaylistCount() {
        this._bindSignal(this.playlists, 'items-changed', () => {
            this._emitPropertyChanged('PlaylistCount');
        }, 'PlaylistCount0');
        return this.playlists.get_n_items();
    }

    get Orderings() {
        return ['Alphabetical', 'ModifiedDate'];
    }

    get ActivePlaylist() {
        this._bindSignal(this.selection, 'notify::playlist', () => {
            this._emitPropertyChanged('ActivePlaylist');
        }, 'ActivePlaylist0');

        let name = this.selection.playlist?.name;
        let displayName = this.playlists.getPlaylist(name)?.displayName || "";
        if (!displayName)
            name = '';

        return [Boolean(name), [this._getPlaylistId(name), displayName, '']];
    }
};

var ServerManager = class {
    constructor(application) {
        this.connection = Gio.DBus.session;

        // Unique name is something like ":1.23456", which is different for each application instance.
        // So each application instance can provide a mpris server.
        let uniqueName = this.connection.get_unique_name().split('.').pop();
        let name = `${BASE_NAME}.${application.application_id}.instance${uniqueName}`;

        this.connection.own_name(name, Gio.BusNameOwnerFlags.NONE, null, null);

        this.servers = [
            new BaseServer(application),
            new PlayerServer(application),
            new TrackListServer(application),
            new PlaylistsServer(application)
        ];

        this._export();
    }

    _export() {
        this.servers.forEach(server => server.export(this.connection, OBJECT_PATH));
    }

    _unexport() {
        this.servers.forEach(server => server.unexport(this.connection));
    }

    disable() {

    }
};
