/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Application */

const { Gio, GLib, GObject, Gtk } = imports.gi;
const { Server: ScannerServer } = imports.scanner;
const { Playlists } = imports.playlists;
const { ApplicationWindow } = imports.window;

const Mpris = imports.mpris;
const ByteArray = imports.byteArray;
const [SIGINT, SIGTERM] = [2, 15];

Uint8Array.prototype.toLiteral = function() {
    return ByteArray.toString(this);
};

// Make list stores iterable.
[Gio.ListStore].forEach(Klass => {
    Klass.prototype[Symbol.iterator] = function*() {
        for (let [i, item] = [0, null]; (item = this.get_item(i)); i++)
            yield item;
    };
});

// Make widgets iterable.
[Gtk.Widget].forEach(Klass => {
    Klass.prototype[Symbol.iterator] = function*() {
        for (let child = this.get_first_child(); child; child = child.get_next_sibling())
            yield child;
    };
});

// Convenient container methods.
[Gtk.Box, Gtk.ListBox].forEach(Klass => {
    let appendOld = Klass.prototype.append;
    let removeOld = Klass.prototype.remove;

    Klass.prototype.append = function(...widgets) {
        widgets.forEach(widget => appendOld.call(this, widget));
    };

    Klass.prototype.with = function(...widgets) {
        this.append(...widgets);
        return this;
    };

    Klass.prototype.remove = function(...widgets) {
        widgets.forEach(widget => removeOld.call(this, widget));
    };

    Klass.prototype.without = function(...widgets) {
        this.remove(...widgets);
        return this;
    };
});

// Accessible attribute setters for widgets.
[
    [Gtk.AccessibleProperty, 'update_property'],
    [Gtk.AccessibleRelation, 'update_relation'],
    [Gtk.AccessibleState, 'update_state'],
].forEach(([enumeration, update]) => {
    for (let key in enumeration) {
        if (key == 'init_value')
            continue;

        let set = function(value) {
            // XXX: Waiting for GJS overrides for accessible lists.
            if (value instanceof Array)
                return;

            this[update]([enumeration[key]], [value]);
        };

        Object.defineProperties(Gtk.Widget.prototype, {
            ['aria-' + key.toLowerCase().replace(/_/g, '')]: { set },
            ['aria_' + key.toLowerCase().replace(/_/g, '')]: { set },
            ['aria' + key[0] + key.slice(1).toLowerCase().replace(/_/g, '')]: { set },
        });
    }
});

// Make accessible attributes usable as property at widget initialization.
Gtk.Widget.prototype._widgetInitOld = Gtk.Widget.prototype._init;
Gtk.Widget.prototype._init = function(params = {}) {
    let ariaEntries = Object.entries(params).filter(([key, value]) => {
        return key.startsWith('aria') && delete params[key];
    });

    this._widgetInitOld(params);

    ariaEntries.forEach(([key, value]) => {
        this[key] = value;
    });
};

// Retrieve the Gtk3 behaviour that filled button accessible label with the icon name.
Gtk.Button.prototype._buttonInitOld = Gtk.Button.prototype._init;
Gtk.Button.prototype._init = function(params) {
    this._buttonInitOld(params);

    let iconName = this.iconName || this.child?.iconName;
    if (iconName)
        this.ariaLabel = iconName;
};

// Make 'widget' property writable at controller initialization.
Gtk.EventController.prototype._eventControllerInitOld = Gtk.EventController.prototype._init;
Gtk.EventController.prototype._init = function(params = {}) {
    let widget = params.widget;
    delete params.widget;

    this._eventControllerInitOld(params);

    if (widget)
        widget.add_controller(this);
};

// Emulate the skipped g_settings_bind_with_mapping() function.
// @getMapping receives a variant and must return [success, property value].
// @setMapping receives a property value and a variant type and must return a variant or null.
// XXX: Tested only with Gio.SettingsBindFlags.GET.
Gio.Settings.prototype.bindWithMapping = function(key, object, property, flags, getMapping = new Function(), setMapping = new Function()) {
    let settingAction = this.create_action(key);

    let bindingFlags = GObject.BindingFlags.SYNC_CREATE;
    if (flags == Gio.SettingsBindFlags.DEFAULT || flags & Gio.SettingsBindFlags.SET)
        bindingFlags |= GObject.BindingFlags.BIDIRECTIONAL;

    let noChanges = false;

    let transformTo = function(binding_, variant) {
        if (noChanges || (flags & Gio.SettingsBindFlags.SET) && !(flags & Gio.SettingsBindFlags.GET))
            return false;

        let [success, value] = getMapping(variant);

        if (success)
            object[property] = value;

        if (flags & Gio.SettingsBindFlags.GET_NO_CHANGES)
            noChanges = true;

        return false;
    };

    let transformFrom = function(binding_, value) {
        let variantType = settingAction.stateType;
        let variant = setMapping(value, variantType);

        if (variant)
            this.set_value(key, variant);

        return false;
    };

    settingAction.bind_property_full('state', object, property, bindingFlags, transformTo, transformFrom);
};

var Application = GObject.registerClass({
    GTypeName: `${pkg.shortName}Application`,
    Properties: {
        'is-primary-instance': GObject.ParamSpec.boolean(
            'is-primary-instance', "Is primary instance", "Whether this is the primary instance",
            GObject.ParamFlags.READWRITE, true
        ),

        'playlists': GObject.ParamSpec.object(
            'playlists', "Playlists", "The playlists",
            GObject.ParamFlags.READWRITE, Playlists.$gtype
        ),
    },
}, class extends Gtk.Application {
    _init() {
        super._init({
            application_id: pkg.name,
            flags: Gio.ApplicationFlags.HANDLES_COMMAND_LINE | Gio.ApplicationFlags.HANDLES_OPEN
        });

        this.add_main_option(
            'version', 'v'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
            _("Print version information and exit"), null
        );

        this.add_main_option(
            'new-window', 'w'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
            _("Open new window"), null
        );

        this.add_main_option(
            'new-instance', 'i'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
            _("Open new instance"), null
        );

        this.add_main_option(
            'recursive', 'r'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
            _("When adding a folder, browse it recursively to add files"), null
        );

        this.add_main_option(
            'scanner', 's'.charCodeAt(0), GLib.OptionFlags.HIDDEN, GLib.OptionArg.NONE,
            '', null
        );
    }

    vfunc_handle_local_options(options) {
        if (this.flags & Gio.ApplicationFlags.IS_SERVICE)
            if (options.contains('scanner')) {
                this.application_id += '.scanner';
                this.scannerServer = new ScannerServer(this);
            }

        if (options.contains('version')) {
            print(pkg.version);
            // Quit the invoked process after printing the version number
            // leaving the running instance unaffected.
            return 0;
        }

        if (options.contains('new-instance')) {
            this.flags |= Gio.ApplicationFlags.NON_UNIQUE;
            this.isPrimaryInstance = false;
        }

        return -1;
    }

    vfunc_command_line(cmdLine) {
        if (cmdLine.get_options_dict().lookup_value('new-window', null))
            new ApplicationWindow({ application: this }).present();
        else
            this.activate();

        let [, ...files] = cmdLine.get_arguments().map(arg => cmdLine.create_file_for_arg(arg));
        if (files.length) {
            let recursive = cmdLine.get_options_dict().lookup_value('recursive', null);
            this.activeWindow.scanner.scanFiles(files, recursive, undefined, true);
        }
    }

    vfunc_open(files, hint_) {
        this.activate();
        this.activeWindow.scanner.scanFiles(files, false, undefined, true);
    }

    vfunc_activate() {
        if (!this.playlists)
            this.playlists = new Playlists();

        (this.activeWindow || new ApplicationWindow({ application: this, playlists: this.playlists })).present();

        if (!this.mprisServerManager)
            this.mprisServerManager = new Mpris.ServerManager(this);
    }

    vfunc_startup() {
        super.vfunc_startup();

        GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, SIGINT, this._destroyAndQuit.bind(this));
        GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, SIGTERM, this._destroyAndQuit.bind(this));

        let action = new Gio.SimpleAction({ name: 'new-window' });
        action.connect('activate', () => {
            new ApplicationWindow({ application: this, playlists: this.playlists }).present();
        });
        this.add_action(action);
        this.set_accels_for_action(`app.new-window`, pkg.shortcutSettings.get_strv('new-window'));
    }

    vfunc_dbus_register(connection, path) {
        if (this.scannerServer)
            this.scannerServer.export(connection, path);

        super.vfunc_dbus_register(connection, path);
        return true;
    }

    _destroyAndQuit() {
        // Some jobs (save persistent playlist ...) are done on window destroyed.
        this.get_windows()
            .filter(window => window instanceof ApplicationWindow)
            .forEach(window => window.close());
        this.quit();
        return GLib.SOURCE_REMOVE;
    }
});
